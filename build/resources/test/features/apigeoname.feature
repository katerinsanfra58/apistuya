Feature: As an user of Geoname I want to get
  the country data of the entered coordinates

  Scenario Outline: get country data successfully
    Given user loads test information
    |formatted | <formateado> |
    | lat      | <latitud>    |
    | lng      | <longitud>   |
    | username | <username>   |
    | style    | <Style>      |
    When user calls Geoname API
    Then user validates response data
    Examples:
     |formateado| latitud | longitud | username       | Style |
     |   true   | -10     | 20       | qa_mobile_easy | full  |



  Scenario Outline: get country data failed
    Given user loads wrong test information
      |formatted | <formateado> |
      | lat      | <latitud>    |
      | lng      | <longitud>   |
      | username | <username>   |
      | style    | <Style>      |
    When user calls Geoname API
    Then user validates that username information is invalid
    Examples:
      |formateado| latitud | longitud | username       | Style |
      |   true   | -10     | 20       | user           | full  |


