package stepdefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import questions.ResponseGeoname;
import questions.WrongUsername;
import tasks.CallGeoname;
import tasks.LoadTestData;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import java.util.Map;

public class ApiGeonameStepDefinitions {
    @Given("^user loads test information$")
    public void userLoadsTestInformation(Map<String, Object> params) {
        theActorInTheSpotlight().wasAbleTo(LoadTestData.loadTestData(params));
    }

    @When("^user calls Geoname API$")
    public void userCallsGeonameAPI() {
        theActorInTheSpotlight().attemptsTo(CallGeoname.callGeoname());
        
    }

    @Then("^user validates response data$")
    public void userValidatesResponseData() {
        theActorInTheSpotlight().should(seeThat(ResponseGeoname.responseGeoname()));
    }


    @Given("^user loads wrong test information$")
    public void userLoadsWrongTestInformation(Map<String, Object> params) {
        theActorInTheSpotlight().wasAbleTo(LoadTestData.loadTestData(params));
        
    }

    @Then("^user validates that username information is invalid$")
    public void userValidatesThatUsernameInformationIsInvalid() {
        theActorInTheSpotlight().should(seeThat(WrongUsername.responseUsername()));
    }
}
