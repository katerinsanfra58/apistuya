package stepdefinitions;

import cucumber.api.java.Before;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.util.EnvironmentVariables;

public class Hook {

    EnvironmentVariables environmentVariables;

    @Before()
    public void preConfiguracion() {
        OnStage.setTheStage(new OnlineCast());
        OnStage.theActorCalled("Cate");
        String baseUrl = environmentVariables.optionalProperty("api.baseUrl")
                                .orElse("http://api.geonames.org/timezoneJSON");
        OnStage.theActorInTheSpotlight().whoCan(CallAnApi.at(baseUrl));
    }
}
