package tasks;

import io.restassured.specification.RequestSpecification;
import models.TestData;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Get;

public class CallGeoname implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Get.resource("").with(request -> request.params(TestData.getTestData()))
        );

    }

    public static CallGeoname callGeoname() {
        return Tasks.instrumented(CallGeoname.class);
    }
}
