package tasks;

import models.TestData;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

import java.util.Map;

public class LoadTestData implements Task {
    private final Map<String, Object> testData;

    public LoadTestData(Map<String, Object> testData) {
        this.testData = testData;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        TestData.setTestData(testData);
    }

    public static LoadTestData loadTestData(Map<String, Object> testData) {
        return Tasks.instrumented(LoadTestData.class, testData);
    }
}
