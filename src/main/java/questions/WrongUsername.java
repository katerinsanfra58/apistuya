package questions;

import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.rest.questions.ResponseConsequence;

import static org.hamcrest.CoreMatchers.equalTo;

public class WrongUsername {


    public static Question<Boolean> responseUsername() {
        return actor -> {
            actor.should(
                    ResponseConsequence.seeThatResponse(response ->
                            response.body("status.message", equalTo("user account not enabled to use the free webservice. Please enable it on your account page: https://www.geonames.org/manageaccount "))

                    )
            );

            return true;
        };
    }
}
