package questions;

import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.rest.questions.ResponseConsequence;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.rest.questions.ResponseConsequence;

import static org.hamcrest.CoreMatchers.equalTo;

public class ResponseGeoname {


    public static Question<Boolean> responseGeoname() {
        return actor -> {
            actor.should(
                    ResponseConsequence.seeThatResponse(response ->
                            response.body("countryName", equalTo("Angola"))
                                    .body("countryCode", equalTo("AO"))
                                    .body("timezoneId", equalTo("Africa/Luanda"))
                    )
            );

            return true;
        };

    }
}
